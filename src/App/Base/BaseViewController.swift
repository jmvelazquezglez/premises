//
//  BaseViewController.swift
//  BaseProject
//
//  Created by José Manuel Velázquez on 13/1/19.
//  Copyright © 2019 José Manuel Velázquez. All rights reserved.
//

import Foundation
import UIKit
#if DEBUG
import FLEX
#endif
import FullMaterialLoader
import SwifterSwift

class BaseViewController: UIViewController, BaseProtocol {
    
    let loader: MaterialLoadingIndicator = MaterialLoadingIndicator(frame: CGRect(x:0, y:0, width: 40, height: 40))
    var viewLoader: UIView?
    
    override func becomeFirstResponder() -> Bool {
        return true
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        
        switch motion {
        case .motionShake:
            #if DEBUG
                FLEXManager.shared()?.showExplorer()
                print("SHOW FLEX")
            #endif
            break
        default:
            break
        }
    }
    
    func loadStyle() {
        loadStyleLoader()
    }
    
    func loadData() {
    }
    
    func loadStyleLoader() {
        loader.indicatorColor = [ColorName.primaryColor.color.cgColor]
    }
    
    func showLoaderInView(_ view: UIView) {
        viewLoader = UIView.init(frame: view.frame)
        viewLoader?.backgroundColor = UIColor.clear
        loader.center = viewLoader!.center
        viewLoader!.addSubview(loader)
        self.view.addSubview(viewLoader!)
        animatedViewLoader(view)
        loader.startAnimating()
    }
    
    func hideLoaderInView(_ view: UIView) {
        animatedViewLoader(view)
        loader.stopAnimating()
        viewLoader?.removeFromSuperview()
    }
    
    func showLoaderOnView(_ view: UIView) {
        viewLoader = UIView.init(frame: view.frame)
        viewLoader?.backgroundColor = UIColor.clear
        loader.center = viewLoader!.center
        viewLoader!.addSubview(loader)
        self.view.addSubview(viewLoader!)
        loader.startAnimating()
    }
    
    func hideLoaderOnView(_ view: UIView) {
        loader.stopAnimating()
        viewLoader?.removeFromSuperview()
    }
    
    private func animatedViewLoader(_ view: UIView) {
        UIView.animate(withDuration: 0.3, animations: {
            view.alpha = view.alpha == 1 ? 0 : 1
        }) { (completed) in
            view.isUserInteractionEnabled = !view.isUserInteractionEnabled
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
}

protocol BaseProtocol {
    
    func loadStyle()
    
    func loadData()
}

protocol BaseCellProtocol {
    
    func loadStyle()
    
    func loadCell()
}
