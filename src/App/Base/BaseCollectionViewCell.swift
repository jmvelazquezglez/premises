//
//  BaseCollectionViewCell.swift
//  BaseProject
//
//  Created by José Manuel Velázquez on 13/04/2019.
//  Copyright © 2019 José Manuel Velázquez. All rights reserved.
//

import Foundation
import UIKit
import SwifterSwift

class BaseCollectionViewCell: UICollectionViewCell, BaseCellProtocol {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func loadStyle() {
        
    }
    
    func loadCell() {
        
    }
}
