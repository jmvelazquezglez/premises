//
//  RestaurantsMapWireFrame.swift
//  Restaurants
//
//  Created by José Manuel Velázquez on 13/04/2019.
//  Copyright © 2019 José Manuel Velázquez. All rights reserved.
//

import Foundation

import UIKit

class PremisesMapWireFrame: PremisesMapWireFrameProtocol {
    
    static func createModule() -> PremisesMapViewController {
        let presenter: PremisesMapPresenterProtocol = PremisesMapPresenter()
        let storyboard: UIStoryboard = UIStoryboard(name: StoryboardScene.Main.storyboardName, bundle: nil)
        let viewController: PremisesMapViewController = storyboard.instantiateViewController(withIdentifier: StoryboardScene.Main.sbRestaurantsMap.identifier) as! PremisesMapViewController
        
        viewController.presenter = presenter
        viewController.presenter?.wireframe = PremisesMapWireFrame()
        viewController.presenter?.viewController = viewController
        
        return viewController
    }
}
