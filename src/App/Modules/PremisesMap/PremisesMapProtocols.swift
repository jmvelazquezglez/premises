//
//  RestaurantsMapProtocols.swift
//  Restaurants
//
//  Created by José Manuel Velázquez on 13/04/2019.
//  Copyright © 2019 José Manuel Velázquez. All rights reserved.
//

import Foundation
import UIKit

protocol PremisesMapViewControllerProtocol: class {
    // PRESENTER -> VIEW
    func showLoader()
    func hideLoader()
    func loadUI()
    func loadController()
    func showError(error: String)
}

protocol PremisesMapPresenterProtocol: class {
    //VIEW -> PRESENTER
    var viewController: PremisesMapViewControllerProtocol? {get set}
    var wireframe: PremisesMapWireFrameProtocol? {get set}
    
    func viewDidLoad()
}

protocol PremisesMapWireFrameProtocol: class {
    //PRESENTER -> WIREFRAME
    static func createModule() -> PremisesMapViewController
}
