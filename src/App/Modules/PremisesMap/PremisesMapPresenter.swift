//
//  RestaurantsMapPresenter.swift
//  Restaurants
//
//  Created by José Manuel Velázquez on 13/04/2019.
//  Copyright © 2019 José Manuel Velázquez. All rights reserved.
//

import Foundation

import UIKit


class PremisesMapPresenter: PremisesMapPresenterProtocol {
    var wireframe: PremisesMapWireFrameProtocol?
    var viewController: PremisesMapViewControllerProtocol?
    var presenter: PremisesMapPresenterProtocol?
//    lazy var interactor: RestaurantsMapInputInteractorProtocol = RestaurantsMapInteractor.init(interactorOutput: self)
    
    func viewDidLoad() {
        viewController?.loadUI()
        viewController?.loadController()
    }
}

//extension RestaurantsMapPresenter: RestaurantsMapOutputInteractorProtocol {
//    
//    
//}

