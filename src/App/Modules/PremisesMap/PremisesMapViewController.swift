//
//  RestaurantsMapViewController.swift
//  Restaurants
//
//  Created by José Manuel Velázquez on 13/04/2019.
//  Copyright © 2019 José Manuel Velázquez. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import IQKeyboardManager
import RealmSwift

enum Info: CGFloat {
    case show = 15, hide = -300
}

enum Transport: Int {
    case walk, car
}

class PremisesMapViewController: BaseViewController,PremisesMapViewControllerProtocol, UITextFieldDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    var locationManager: CLLocationManager = CLLocationManager()
    @IBOutlet weak var viewPoint: UIView!
    @IBOutlet weak var btnWeb: UIButton!
    @IBOutlet weak var btnRoute: UIButton!
    @IBOutlet weak var lbNameRestaurant: UILabel!
    @IBOutlet weak var lbPhoneRestaurant: UILabel!
    @IBOutlet weak var viewWeb: UIView!
    @IBOutlet weak var segmentedTransport: UISegmentedControl!
    @IBOutlet weak var constraintInfo: NSLayoutConstraint!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var btnFavourite: UIButton!
    @IBOutlet weak var viewRoute: UIView!
    @IBOutlet weak var lbDistance: UILabel!
    @IBOutlet weak var btnCancelRoute: UIButton!
    @IBOutlet weak var constraintRoute: NSLayoutConstraint!
    @IBOutlet weak var segmentedMap: UISegmentedControl!
    
    var loadedFavourites = false
    var loadedUser = false
    var loadedRoute = false
    var startRoute = false
    
    let realm = try! Realm()
    lazy var premises: Results<Premise> = { self.realm.objects(Premise.self) }()
    
    var meterLocalicePremises: CLLocationDistance = 50000
    var zoomMap = 14
    var locationUser: CLLocation?
    
    var premiseInfo = PremisesAnnotation()
    var annotationViewSelected: MKAnnotationView?
    
    var presenter:PremisesMapPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadUI() {
        super.loadStyle()
        
        title = L10n.mapTitle
        
        navigationController?.loadStyleNavigationWithColor(ColorName.primaryColor.color)
        
        mapView.loadStyleMapPrimaryColor()
        mapView.showsUserLocation = true
        
        viewPoint.cornerRadius(6)
        viewPoint.shadowBottom()
        
        viewRoute.cornerRadius(6)
        viewRoute.shadowBottom()
        
        lbNameRestaurant.loadStyleLabelColorBlack()
        lbNameRestaurant.loadStyleLabelFontBold()
        lbNameRestaurant.loadStyleLabelSize16()
        
        lbPhoneRestaurant.loadStyleLabelColorBlack()
        lbPhoneRestaurant.loadStyleLabelFontRegular()
        lbPhoneRestaurant.loadStyleLabelSize14()
        
        lbDistance.loadStyleLabelColorBlack()
        lbDistance.loadStyleLabelFontRegular()
        lbDistance.loadStyleLabelSize14()
        
        btnWeb.setTitle(L10n.mapWeb, for: UIControl.State.normal)
        btnWeb.loadStyleButtonColorPrimary()
        btnWeb.cornerRadius(4)
        btnWeb.shadowBottom()
        
        btnRoute.setTitle(L10n.mapRoute, for: UIControl.State.normal)
        btnRoute.loadStyleButtonColorPrimary()
        btnRoute.cornerRadius(4)
        btnRoute.shadowBottom()
        
        btnCancelRoute.setTitle(L10n.routeCancel, for: UIControl.State.normal)
        btnCancelRoute.loadStyleButtonColorPrimary()
        btnCancelRoute.cornerRadius(4)
        btnCancelRoute.shadowBottom()
        
        btnFavourite.setTitle("", for: UIControl.State.normal)
        
        loadSegmentTransport()
        loadSegmentMap()
        loadTfSearch()
    }
    
    func loadSegmentTransport() {
        segmentedTransport.removeAllSegments()
        segmentedTransport.loadStyleSegmentPrimaryColor()
        segmentedTransport.insertSegment(withTitle: L10n.transportWalk, at: Transport.walk.rawValue, animated: true)
        segmentedTransport.insertSegment(withTitle: L10n.transportCar, at: Transport.car.rawValue, animated: true)
        segmentedTransport.selectedSegmentIndex = 0
    }
    
    func loadSegmentMap() {
        segmentedMap.removeAllSegments()
        segmentedMap.loadStyleSegmentPrimaryColor()
        segmentedMap.insertSegment(withTitle: L10n.mapTypeStandard, at: Int(MKMapType.standard.rawValue), animated: true)
        segmentedMap.insertSegment(withTitle: L10n.mapTypeSatellite, at: Int(MKMapType.satellite.rawValue), animated: true)
        segmentedMap.selectedSegmentIndex = 0
    }
    
    func loadTfSearch() {
        tfSearch.loadStyleTextFieldSearch()
        tfSearch.delegate = self
        
        tfSearch.text = ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        search(text: textField.text!)
        return true
    }
    
    func search(text: String) {
        if text.isEmpty == false {
            if startRoute {
                actionCancelRoute((Any).self)
            }
            
            loadPlace(place: text)
        }else {
            for annotation in mapView.annotations {
                if annotation.isEqual(premiseInfo) == false {
                    mapView.removeAnnotation(annotation)
                }
            }
        }
        tfSearch.resignFirstResponder()
    }
    
    func loadController() {
        updatePositionInfo(position: Info.hide.rawValue)
        updatePositionRoute(position: Info.hide.rawValue)
        loadUserLocation()
    }
    
    func showLoader() {
        view.isUserInteractionEnabled = false
        showLoaderOnView(mapView)
    }
    
    func hideLoader() {
        view.isUserInteractionEnabled = true
        hideLoaderOnView(mapView)
    }
    
    func showError(error: String) {
        
    }
    
    @IBAction func actionWeb(_ sender: Any) {
        webRestaurant()
    }
    
    @IBAction func actionRoute(_ sender: Any) {
        startRoute = true
        loadedRoute = false
        hideInfo()
        showLoader()
        loadRoutePemise(transport: Transport(rawValue: segmentedTransport.selectedSegmentIndex)!)
    }
    
    @IBAction func actionTransport(_ sender: UISegmentedControl) {
        
    }
    
    @IBAction func actionFavourite(_ sender: Any) {
        checkFavourite(annotation: premiseInfo)
    }
    
    @IBAction func actionCancelRoute(_ sender: Any) {
        startRoute = false
        
        mapView.selectAnnotation(premiseInfo, animated: true)
        
        hideRoute()
        clearMapOverlays()
        showInfo()
    }
    
    @IBAction func actionChangeMapType(_ sender: UISegmentedControl) {
        mapView.mapType = MKMapType(rawValue: MKMapType.RawValue(sender.selectedSegmentIndex))!
    }
}

// MARK: Mapa

extension PremisesMapViewController: MKMapViewDelegate, CLLocationManagerDelegate {
    
    func loadUserLocation() {
        mapView.delegate = self
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationUser = locations.first
        
        self.locationUser = locationUser
        showFavouritesAnnotation()
        
        if loadedUser == false {
            loadedUser = true
            mapView.setCenterCoordinate(coordinate: locationUser!.coordinate, zoomLevel: zoomMap, animated: true)
        }
        
        if mapView.overlays.count > 0 {
            loadRoutePemise(transport: Transport(rawValue: segmentedTransport.selectedSegmentIndex)!)
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view.annotation!.isKind(of: MKUserLocation.self) {
            return
        }
        
        if startRoute == false {
            
            tfSearch.resignFirstResponder()
            annotationViewSelected = view
            let annotation: PremisesAnnotation = view.annotation as! PremisesAnnotation
            premiseInfo = annotation
            mapView.setCenterCoordinate(coordinate: annotation.coordinate, zoomLevel: zoomMap, animated: true)
            
            updateInfo(info: annotation)
            
            showInfo()
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if view.annotation!.isKind(of: MKUserLocation.self) {
            return
        }
        
        let annotation: PremisesAnnotation = view.annotation as! PremisesAnnotation
        view.image = annotation.imagePremise
        view.centerOffset = CGPoint(x: 0, y: -(view.image?.size.height)! / 2);
        
        hideInfo()
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        let annotationView = mapView.view(for: userLocation)
        annotationView?.canShowCallout = false
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation.isKind(of: MKUserLocation.self) {
            return nil
        }
        
        var premise: PremisesAnnotation?
        
        if isFavourite(annotation: annotation as! MKPointAnnotation) == true {
            premise = premiseToAnnotation(annotation: annotation as! MKPointAnnotation)
        }else {
            premise = (annotation as! PremisesAnnotation)
        }
        
        let annotationView = MKAnnotationView.init(annotation: annotation, reuseIdentifier: "Premise")
        annotationView.canShowCallout = false
        annotationView.image = premise!.imagePremise
        annotationView.centerOffset = CGPoint(x: 0, y: -(annotationView.image?.size.height)! / 2);
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        polylineRenderer.loadStylePolyLineColorPrimary()
        return polylineRenderer
    }
    
    func clearMapAnnotations() {
        for annotation in mapView.annotations {
            if annotation.isKind(of: MKUserLocation.self) == false && isFavourite(annotation: annotation as! MKPointAnnotation) == false {
                mapView.removeAnnotation(annotation)
            }
        }
    }
    
    func clearMapOverlays() {
        mapView.removeOverlays(mapView.overlays)
    }
}

// MARK: Búsqueda

extension PremisesMapViewController {
    
    func loadPlace(place: String) {
        
        showLoader()
        clearMapAnnotations()
        clearMapOverlays()
        
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = place
        request.region = MKCoordinateRegion(center: locationUser!.coordinate, latitudinalMeters: meterLocalicePremises, longitudinalMeters: meterLocalicePremises)
        
        let search = MKLocalSearch(request: request)
        search.start { response, error in
            guard let response = response else {
                return
            }
            
            for item in response.mapItems {
                print(item)
                let annotation = PremisesAnnotation.initAnnotation(coordinate: CLLocationCoordinate2DMake(item.placemark.coordinate.latitude, item.placemark.coordinate.longitude))
                
                annotation.loadAnnotation(name: item.name, phone: item.phoneNumber, url: item.url, coordinate: CLLocationCoordinate2DMake(item.placemark.coordinate.latitude, item.placemark.coordinate.longitude), image: Asset.pin.image.maskWithColor(color: ColorName.primaryColor.color), imageBig: Asset.pinBig.image.maskWithColor(color: ColorName.primaryColor.color))
                
                if self.isFavourite(annotation: annotation) == false {
                    self.mapView.addAnnotation(annotation)
                }
            }
            
            self.hideLoader()
            self.mapView.showAnnotations(self.mapView.annotations, animated: true)
        }
    }
    
    func showInfo() {
        updatePositionInfo(position: Info.show.rawValue)
    }
    
    func hideInfo() {
        updatePositionInfo(position: Info.hide.rawValue)
    }
    
    func showRoute() {
        updatePositionRoute(position: Info.show.rawValue)
    }
    
    func hideRoute() {
        updatePositionRoute(position: Info.hide.rawValue)
    }
    
    func updatePositionInfo(position: CGFloat) {
        constraintInfo.constant = position
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func updatePositionRoute(position: CGFloat) {
        constraintRoute.constant = position
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func updateInfo(info: PremisesAnnotation) {
        lbNameRestaurant.text = info.namePremise
        lbPhoneRestaurant.text = info.phonePremise
        viewWeb.isHidden = info.urlPremise == nil
        annotationViewSelected!.image = info.imageBigPremise
        annotationViewSelected!.centerOffset = CGPoint(x: 0, y: -(annotationViewSelected!.image?.size.height)! / 2);
        
        if isFavourite(annotation: info) {
            btnFavourite.setImage(Asset.favourite.image.maskWithColor(color: ColorName.primaryColor.color), for: UIControl.State.normal)
        }else {
            btnFavourite.setImage(Asset.noFavourite.image.maskWithColor(color: ColorName.primaryColor.color), for: UIControl.State.normal)
        }
    }
    
    func webRestaurant() {
        UIApplication.shared.open(premiseInfo.urlPremise!, options: [:]) { (completion) in
        }
    }
    
    func loadRoutePemise(transport: Transport) {
        
        mapView.deselectAnnotation(premiseInfo, animated: true)
        
        let request = MKDirections.Request()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: locationUser!.coordinate, addressDictionary: nil))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: premiseInfo.coordinatePremise!, addressDictionary: nil))
        request.requestsAlternateRoutes = true
        
        switch transport {
        case .walk:
            request.transportType = .walking
        case .car:
            request.transportType = .automobile
        }
        
        let directions = MKDirections(request: request)
        
        directions.calculate { [unowned self] response, error in
            guard let response = response else { return }
            
            if (response.routes.count > 0) {
                
                if self.startRoute == true {
                    self.mapView.addOverlay(response.routes[0].polyline)
                    
                    self.lbDistance.text = self.premiseInfo.getDistance(distance: response.routes[0].distance, time: Int(response.routes[0].expectedTravelTime))
                    
                    if self.mapView.overlays.count > 1 {
                        self.mapView.removeOverlay(self.mapView.overlays.first!)
                    }
                    
                    if self.loadedRoute == false {
                        self.showRoute()
                        self.hideLoader()
                        self.loadedRoute = true
                        self.mapView.setVisibleMapRect(response.routes[0].polyline.boundingMapRect, animated: true)
                    }
                }else {
                    self.mapView.removeOverlays(self.mapView.overlays)
                }
            }
        }
    }
}

// MARK: Guardar

extension PremisesMapViewController {
    
    func checkFavourite(annotation: PremisesAnnotation) {
        if isFavourite(annotation: annotation) {
            annotation.imagePremise = Asset.pin.image.maskWithColor(color: ColorName.primaryColor.color)
            annotation.imageBigPremise = Asset.pinBig.image.maskWithColor(color: ColorName.primaryColor.color)
            deleteFavourite(annotation: annotation)
        }else {
            annotation.imagePremise = Asset.favourite.image.maskWithColor(color: ColorName.favourite.color)
            annotation.imageBigPremise = Asset.favouriteBig.image.maskWithColor(color: ColorName.favourite.color)
            addFavourite(annotation: annotation)
        }
        
        updateInfo(info: annotation)
    }
    
    func deleteFavourite(annotation: PremisesAnnotation) {
        for premise in premises {
            
            let exist = (premise.latitude == annotation.coordinate.latitude && premise.longitude == annotation.coordinate.longitude)
            
            if exist == true {
                try! realm.write() {
                    realm.delete(premise)
                }
                break
            }
        }
    }
    
    func addFavourite(annotation: PremisesAnnotation) {
        let premise: Premise = Premise()
        premise.namePremise = annotation.namePremise!
        premise.phonePremise = annotation.phonePremise!
       
        if annotation.urlPremise != nil {
            premise.urlPremise = annotation.urlPremise!.absoluteString
        }
        
        premise.latitude = annotation.coordinatePremise!.latitude
        premise.longitude = annotation.coordinatePremise!.longitude
        
        try! realm.write() {
            realm.add(premise)
        }
    }
    
    func showFavouritesAnnotation() {
        if loadedFavourites == false {
            loadedFavourites = true
            for premise in premises {
                let annotation = createPremiseAnnotationFavourite(premise: premise)
                annotation.coordinate = CLLocationCoordinate2DMake(premise.latitude, premise.longitude)
                mapView.addAnnotation(annotation)
            }
            
            mapView.showAnnotations(mapView.annotations, animated: true)
        }
    }
    
    func isFavourite(annotation: MKPointAnnotation) -> Bool {
        var favourite = false
        
        for premise in premises {
            
            favourite = (premise.latitude == annotation.coordinate.latitude && premise.longitude == annotation.coordinate.longitude)
            
            if favourite == true {
                break
            }
        }
        
        return favourite
    }
    
    func premiseToAnnotation(annotation: MKPointAnnotation) -> PremisesAnnotation{
        
        var premiseAnnotation: PremisesAnnotation?
        
        for premise in premises {
            
            let favourite = (premise.latitude == annotation.coordinate.latitude && premise.longitude == annotation.coordinate.longitude)
            
            if favourite == true {
                premiseAnnotation = createPremiseAnnotationFavourite(premise: premise)
                break
            }
        }
        
        return premiseAnnotation!
    }
    
    func createPremiseAnnotationFavourite(premise: Premise) -> PremisesAnnotation {
        
        var url: URL?
        
        if premise.urlPremise.isEmpty == false {
            url = URL(string: premise.urlPremise)
        }
        
        let premiseAnnotation = PremisesAnnotation.initAnnotation(coordinate: CLLocationCoordinate2DMake(premise.latitude, premise.longitude))
        
        premiseAnnotation.loadAnnotation(name: premise.namePremise, phone: premise.phonePremise, url: url, coordinate: CLLocationCoordinate2DMake(premise.latitude, premise.longitude), image: Asset.favourite.image.maskWithColor(color: ColorName.favourite.color), imageBig: Asset.favouriteBig.image.maskWithColor(color: ColorName.favourite.color))
        
        return premiseAnnotation
    }
}
