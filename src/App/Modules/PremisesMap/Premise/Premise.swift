//
//  Premise.swift
//  Premises
//
//  Created by José Manuel Velázquez on 15/04/2019.
//  Copyright © 2019 José Manuel Velázquez. All rights reserved.
//

import Foundation
import RealmSwift
import MapKit

class Premise: Object {
    @objc dynamic var namePremise = ""
    @objc dynamic var phonePremise = ""
    @objc dynamic var distancePremise = ""
    @objc dynamic var urlPremise = ""
    @objc dynamic var latitude = 0.0
    @objc dynamic var longitude = 0.0
}
