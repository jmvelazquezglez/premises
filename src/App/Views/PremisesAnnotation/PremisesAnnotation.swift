//
//  RestaurantAnnotation.swift
//  Restaurants
//
//  Created by José Manuel Velázquez on 14/04/2019.
//  Copyright © 2019 José Manuel Velázquez. All rights reserved.
//

import UIKit
import MapKit

class PremisesAnnotation: MKPointAnnotation {

    var namePremise: String?
    var phonePremise: String?
    var urlPremise: URL?
    var coordinatePremise: CLLocationCoordinate2D?
    var imagePremise: UIImage?
    var imageBigPremise: UIImage?
    
    class func initAnnotation(coordinate: CLLocationCoordinate2D) -> PremisesAnnotation {
        let annotation = PremisesAnnotation.init()
        annotation.coordinate = coordinate
        
        return annotation
    }
    
    func loadAnnotation(name: String?, phone: String?, url: URL?, coordinate: CLLocationCoordinate2D?, image: UIImage?, imageBig: UIImage?) {
        
        self.namePremise = name
        self.phonePremise = phone
        self.urlPremise = url
        self.coordinatePremise = coordinate
        self.imagePremise = image
        self.imageBigPremise = imageBig
    }
    
    func getDistance(distance: Double, time: Int) -> String {
        
        var strTime = "";
        
        let seconds = time
        let minutes = (time / 60) % 60
        let hours = time / 3600
        
        let strHour = (hours == 1) ? L10n.mapHour : L10n.mapHours
        let strMinutes = (minutes == 1) ? L10n.mapMinute : L10n.mapMinutes
        let strSeconds = (seconds == 1) ? L10n.mapSecond : L10n.mapSeconds
        
        if (hours > 0 && minutes == 0) {
            strTime = String(format: "%d %@", hours, strHour)
        }else if (hours > 0 && minutes > 0) {
            strTime = String(format: "%d %@ %d %@", hours, strHour, minutes, strMinutes)
        }else if (hours == 0 && minutes > 0) {
            strTime = String(format: "%d %@", minutes, strMinutes)
        }else if (minutes == 0 && seconds > 0) {
            strTime = String(format: "%d %@", seconds, strSeconds)
        }else {
            strTime = String(format: "0%@", strSeconds)
        }
        
        var strMeters: String = ""

        if distance >= 1000 {
            strMeters = String(format: "%@ %.2f km", L10n.distanceTo, CGFloat(distance) / 1000)
        }else {
            strMeters = String(format: "%@ %.2f m", L10n.distanceTo, CGFloat(distance))
        }

        return String(format: "%@ \n\n %@", strMeters.replacingOccurrences(of: ".", with: ","), strTime)
    }
}
