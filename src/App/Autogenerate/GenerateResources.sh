
#!/bin/sh

#https://github.com/SwiftGen/SwiftGen

RSROOT=$SRCROOT/App/Others/Resources
OUTPUTRSROOT=$SRCROOT/App/Others/Constants

"$PODS_ROOT/SwiftGen/bin/swiftgen" xcassets -t swift3 "$RSROOT/Assets.xcassets" --output "$OUTPUTRSROOT/Images.swift"

"$PODS_ROOT/SwiftGen/bin/swiftgen" strings -t structured-swift4 "$RSROOT/Localizables/Base.lproj/Localizable.strings" --output "$OUTPUTRSROOT/LS.swift"

"$PODS_ROOT/SwiftGen/bin/swiftgen" colors -t swift4 "$RSROOT/Colors/Colors.json" --output "$OUTPUTRSROOT/Colors.swift"

"$PODS_ROOT/SwiftGen/bin/swiftgen" fonts -t swift4 "$RSROOT/Fonts" --output "$OUTPUTRSROOT/Fonts.swift"

"$PODS_ROOT/SwiftGen/bin/swiftgen" storyboards -t scenes-swift4 "$RSROOT/Storyboards" --output "$OUTPUTRSROOT/SB.swift"

