//
//  Style.swift
//  Drink
//
//  Created by José Manuel Velázquez on 17/12/18.
//  Copyright © 2018 José Manuel Velázquez. All rights reserved.
//

import UIKit
import SwifterSwift
import MapKit

class Style: NSObject {

}

extension UIView {
    func cornerRadius(_ radius: Float) {
        layer.cornerRadius = CGFloat(radius)
        clipsToBounds = true
    }
    
    func shadowBottom() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowOffset = CGSize(width: 3, height: 3)
        layer.shadowRadius = 3
        layer.masksToBounds = false
    }
}

extension UITextField {
    
    func loadStyleTextFieldSearch() {
        self.cornerRadius(2)
        backgroundColor = UIColor.white
        textColor = UIColor.black
        font = UIFont(name: FontFamily.OpenSans.regular.name, size: 14)
        shadowBottom()
        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 1
        
        let padding = UIView.init(frame: CGRect(x: 0, y: 0, width: 10, height: frame.size.height))
        leftView = padding
        leftViewMode = .always
        rightView = padding
        rightViewMode = .always
        
        
        let attributesDictionary = [NSAttributedString.Key.foregroundColor: UIColor.black.withAlphaComponent(0.7), NSAttributedString.Key.font: font]
        attributedPlaceholder = NSAttributedString(string: L10n.mapSearch, attributes: attributesDictionary as [NSAttributedString.Key : Any])
    }
}

extension UILabel {
    
    func loadStyleLabelColorBlack() {
        textColor = UIColor.black
    }
    
    func loadStyleLabelColorWhite() {
        textColor = UIColor.white
    }
    
    func loadStyleLabelColorPrimary() {
        textColor = ColorName.primaryColor.color
    }
    
    func loadStyleLabelFontBold() {
        font = UIFont(name: FontFamily.OpenSans.bold.name, size: font.pointSize)
    }
    
    func loadStyleLabelFontRegular() {
        font = UIFont(name: FontFamily.OpenSans.regular.name, size: font.pointSize)
    }
    
    func loadStyleLabelFontItalic() {
        font = UIFont(name: FontFamily.OpenSans.italic.name, size: font.pointSize)
    }
    
    func loadStyleLabelSize14() {
        font = UIFont(name: font.fontName, size: 14)
    }
    
    func loadStyleLabelSize16() {
        font = UIFont(name: font.fontName, size: 16)
    }
}

extension UIButton {
    
    func loadStyleButtonColorPrimary() {
        titleLabel?.loadStyleLabelSize14()
        titleLabel?.loadStyleLabelFontRegular()
        setTitleColor(UIColor.white, for: UIControl.State.normal)
        
        backgroundColor = ColorName.primaryColor.color
    }
}

extension UINavigationController {
    
    fileprivate func colorForNavigation(_ color: UIColor) {
        navigationBar.barTintColor = color
        navigationBar.tintColor = color
        view.backgroundColor = color
        navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationBar.isTranslucent = false
    }
    
    func loadStyleTransparentNavigation() {
        colorForNavigation(.clear)
    }
    
    func loadStyleNavigationWithColor(_ color: UIColor) {
        colorForNavigation(color)
    }
}

extension MKMapView {
    
    func loadStyleMapPrimaryColor() {
        tintColor = ColorName.primaryColor.color
    }
}

extension UISegmentedControl {
    func loadStyleSegmentPrimaryColor() {
        backgroundColor = UIColor.white
        layer.cornerRadius = 4
        tintColor = ColorName.primaryColor.color
    }
}

extension MKPolylineRenderer {
    
    func loadStylePolyLineColorPrimary() {
        strokeColor = ColorName.primaryColor.color
        lineWidth = 3;
    }
}

extension UIImage {
    
    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
    
}
