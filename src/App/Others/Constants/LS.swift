// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
internal enum L10n {
  /// To
  internal static let distanceTo = L10n.tr("Localizable", "distance-to")
  /// hour
  internal static let mapHour = L10n.tr("Localizable", "map-hour")
  /// hours
  internal static let mapHours = L10n.tr("Localizable", "map-hours")
  /// minute
  internal static let mapMinute = L10n.tr("Localizable", "map-minute")
  /// minutes
  internal static let mapMinutes = L10n.tr("Localizable", "map-minutes")
  /// Route
  internal static let mapRoute = L10n.tr("Localizable", "map-route")
  /// Search
  internal static let mapSearch = L10n.tr("Localizable", "map-search")
  /// second
  internal static let mapSecond = L10n.tr("Localizable", "map-second")
  /// seconds
  internal static let mapSeconds = L10n.tr("Localizable", "map-seconds")
  /// Where are we going today?
  internal static let mapTitle = L10n.tr("Localizable", "map-title")
  /// Web
  internal static let mapWeb = L10n.tr("Localizable", "map-web")
  /// Satellite
  internal static let mapTypeSatellite = L10n.tr("Localizable", "map_type-satellite")
  /// Standard
  internal static let mapTypeStandard = L10n.tr("Localizable", "map_type-standard")
  /// Cancel
  internal static let routeCancel = L10n.tr("Localizable", "route-cancel")
  /// Car
  internal static let transportCar = L10n.tr("Localizable", "transport-car")
  /// Walk
  internal static let transportWalk = L10n.tr("Localizable", "transport-walk")
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    // swiftlint:disable:next nslocalizedstring_key
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
